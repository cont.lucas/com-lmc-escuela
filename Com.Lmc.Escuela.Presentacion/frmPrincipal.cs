﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Com.Lmc.Escuela.Presentacion {
    public partial class frmPrincipal : Form {
        public frmPrincipal() {
            InitializeComponent();
        }

        private void tmnuMaestrosAlumno_Click(object sender, EventArgs e) {
            var fla = new Forms.Alumnos.frmListaAlumnos();
            fla.MdiParent = this;
            fla.Show();
        }
    }
}
