﻿namespace Com.Lmc.Escuela.Presentacion {
    partial class frmPrincipal {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.mnuPrincipal = new System.Windows.Forms.MenuStrip();
            this.tmnuMaestros = new System.Windows.Forms.ToolStripMenuItem();
            this.tmnuMaestrosEscuela = new System.Windows.Forms.ToolStripMenuItem();
            this.tmnuMaestrosAlumno = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // mnuPrincipal
            // 
            this.mnuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmnuMaestros});
            this.mnuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.mnuPrincipal.Name = "mnuPrincipal";
            this.mnuPrincipal.Size = new System.Drawing.Size(800, 24);
            this.mnuPrincipal.TabIndex = 0;
            this.mnuPrincipal.Text = "menuStrip1";
            // 
            // tmnuMaestros
            // 
            this.tmnuMaestros.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tmnuMaestrosEscuela,
            this.tmnuMaestrosAlumno});
            this.tmnuMaestros.Name = "tmnuMaestros";
            this.tmnuMaestros.Size = new System.Drawing.Size(63, 20);
            this.tmnuMaestros.Text = "Maestros";
            // 
            // tmnuMaestrosEscuela
            // 
            this.tmnuMaestrosEscuela.Name = "tmnuMaestrosEscuela";
            this.tmnuMaestrosEscuela.Size = new System.Drawing.Size(115, 22);
            this.tmnuMaestrosEscuela.Text = "Escuelas";
            // 
            // tmnuMaestrosAlumno
            // 
            this.tmnuMaestrosAlumno.Name = "tmnuMaestrosAlumno";
            this.tmnuMaestrosAlumno.Size = new System.Drawing.Size(115, 22);
            this.tmnuMaestrosAlumno.Text = "Alumnos";
            this.tmnuMaestrosAlumno.Click += new System.EventHandler(this.tmnuMaestrosAlumno_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.mnuPrincipal);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mnuPrincipal;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Escuela";
            this.mnuPrincipal.ResumeLayout(false);
            this.mnuPrincipal.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip mnuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem tmnuMaestros;
        private System.Windows.Forms.ToolStripMenuItem tmnuMaestrosEscuela;
        private System.Windows.Forms.ToolStripMenuItem tmnuMaestrosAlumno;
    }
}

