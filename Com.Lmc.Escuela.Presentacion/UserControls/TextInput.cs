﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Com.Lmc.Escuela.Presentacion.UserControls {
    public partial class TextInput : UserControl {
        private bool _isValid = true;

        public TextInput() {
            InitializeComponent();
        }

        public bool OnlyNumbers { get; set; }
        public bool IsOptional { get; set; } = false;
        public bool IsValid {
            get { return this._isValid; }
            set {
                if (!IsOptional) {
                    if (value == false) {
                        this.BackColor = Color.Salmon;
                    }
                    else {
                        this.BackColor = Color.Transparent;
                    }
                }

                this._isValid = value;
            }
        }
        public bool ActiveEnterTab { get; set; } = true;
        public int MaxLength {
            get {
                return this.txtInput.MaxLength;
            }
            set {
                this.txtInput.MaxLength = value;
            }
        }
        public string Value {
            get { return this.txtInput.Text; }
            set { this.txtInput.Text = value; }
        }
        public bool HasAnyValue() {
            if (IsOptional) {
                return true;
            }

            return !(String.IsNullOrEmpty(this.txtInput.Text)
                || String.IsNullOrWhiteSpace(this.txtInput.Text));
        }

        public ControlBindingsCollection DataBindingsTextInput {
            get { return this.txtInput.DataBindings; }
        }

        public TextBox CurrentInput {
            get { return this.txtInput; }
        }

        private void TextInput_Load(object sender, EventArgs e) {
            ConfigureControl();
        }

        private void ConfigureControl() {
            if (IsOptional) {
                this.txtInput.BackColor = System.Drawing.SystemColors.Info;
            }

            if (OnlyNumbers) {
                this.txtInput.TextChanged += this.txtInput_OnlyNumbers;
                this.txtInput.KeyPress += this.txtInput_OnlyNumberKeys;
            }

            this.txtInput.MaxLength = this.MaxLength;
        }

        private void txtInput_OnlyNumbers(object sender, EventArgs e) {
            var txt = sender as TextBox;
            if (OnlyNumbers) {
                Int32.TryParse(txt.Text, out int result);

                if (result == 0) {
                    txt.Text = "";
                }
            }
        }

        private void txtInput_OnlyNumberKeys(object sender, KeyPressEventArgs e) {
            var txt = sender as TextBox;
            if (OnlyNumbers) {
                if (Char.IsDigit(e.KeyChar)) {
                    e.Handled = false;
                }
                else {
                    if (Char.IsControl(e.KeyChar)) {
                        e.Handled = false;
                    }
                    else {
                        e.Handled = true;
                    }
                }
            }
        }

        private void txtInput_KeyDown(object sender, KeyEventArgs e) {
            if (ActiveEnterTab) {
                if (e.KeyCode == Keys.Enter) {
                    SendKeys.SendWait("{tab}");
                }
            }
        }
    }
}
