﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Lmc.Escuela.Presentacion.Utils {
    public class Utils {
        public static bool IsEmptyString(string value) {
            return String.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value);
        }
    }
}
