﻿namespace Com.Lmc.Escuela.Presentacion.Forms.Alumnos {
    partial class frmAlumnos {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.grbDatosAlumno = new System.Windows.Forms.GroupBox();
            this.lblFDN = new System.Windows.Forms.Label();
            this.dtpFDN = new System.Windows.Forms.DateTimePicker();
            this.lblDni = new System.Windows.Forms.Label();
            this.lblEdad = new System.Windows.Forms.Label();
            this.lblApellido = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.grbDomicilio = new System.Windows.Forms.GroupBox();
            this.lblDepto = new System.Windows.Forms.Label();
            this.lblPiso = new System.Windows.Forms.Label();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblCalle = new System.Windows.Forms.Label();
            this.grbComandos = new System.Windows.Forms.GroupBox();
            this.txtDepto = new Com.Lmc.Escuela.Presentacion.UserControls.TextInput();
            this.txtPiso = new Com.Lmc.Escuela.Presentacion.UserControls.TextInput();
            this.txtNumero = new Com.Lmc.Escuela.Presentacion.UserControls.TextInput();
            this.txtCalle = new Com.Lmc.Escuela.Presentacion.UserControls.TextInput();
            this.txtApellido = new Com.Lmc.Escuela.Presentacion.UserControls.TextInput();
            this.txtNombre = new Com.Lmc.Escuela.Presentacion.UserControls.TextInput();
            this.txtDni = new Com.Lmc.Escuela.Presentacion.UserControls.TextInput();
            this.txtEdad = new Com.Lmc.Escuela.Presentacion.UserControls.TextInput();
            this.chkActivo = new System.Windows.Forms.CheckBox();
            this.grbDatosAlumno.SuspendLayout();
            this.grbDomicilio.SuspendLayout();
            this.grbComandos.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbDatosAlumno
            // 
            this.grbDatosAlumno.Controls.Add(this.chkActivo);
            this.grbDatosAlumno.Controls.Add(this.txtApellido);
            this.grbDatosAlumno.Controls.Add(this.txtNombre);
            this.grbDatosAlumno.Controls.Add(this.txtDni);
            this.grbDatosAlumno.Controls.Add(this.txtEdad);
            this.grbDatosAlumno.Controls.Add(this.lblFDN);
            this.grbDatosAlumno.Controls.Add(this.dtpFDN);
            this.grbDatosAlumno.Controls.Add(this.lblDni);
            this.grbDatosAlumno.Controls.Add(this.lblEdad);
            this.grbDatosAlumno.Controls.Add(this.lblApellido);
            this.grbDatosAlumno.Controls.Add(this.lblNombre);
            this.grbDatosAlumno.Location = new System.Drawing.Point(12, 12);
            this.grbDatosAlumno.Name = "grbDatosAlumno";
            this.grbDatosAlumno.Size = new System.Drawing.Size(335, 175);
            this.grbDatosAlumno.TabIndex = 0;
            this.grbDatosAlumno.TabStop = false;
            this.grbDatosAlumno.Text = "Datos del alumno";
            // 
            // lblFDN
            // 
            this.lblFDN.AutoSize = true;
            this.lblFDN.Location = new System.Drawing.Point(146, 100);
            this.lblFDN.Name = "lblFDN";
            this.lblFDN.Size = new System.Drawing.Size(111, 13);
            this.lblFDN.TabIndex = 8;
            this.lblFDN.Text = "Fecha de Nacimiento:";
            // 
            // dtpFDN
            // 
            this.dtpFDN.CustomFormat = "dd-MM-yyyy";
            this.dtpFDN.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFDN.Location = new System.Drawing.Point(149, 113);
            this.dtpFDN.Name = "dtpFDN";
            this.dtpFDN.Size = new System.Drawing.Size(126, 20);
            this.dtpFDN.TabIndex = 5;
            // 
            // lblDni
            // 
            this.lblDni.AutoSize = true;
            this.lblDni.Location = new System.Drawing.Point(8, 100);
            this.lblDni.Name = "lblDni";
            this.lblDni.Size = new System.Drawing.Size(26, 13);
            this.lblDni.TabIndex = 5;
            this.lblDni.Text = "DNI";
            // 
            // lblEdad
            // 
            this.lblEdad.AutoSize = true;
            this.lblEdad.Location = new System.Drawing.Point(87, 100);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(32, 13);
            this.lblEdad.TabIndex = 7;
            this.lblEdad.Text = "Edad";
            // 
            // lblApellido
            // 
            this.lblApellido.AutoSize = true;
            this.lblApellido.Location = new System.Drawing.Point(8, 60);
            this.lblApellido.Name = "lblApellido";
            this.lblApellido.Size = new System.Drawing.Size(44, 13);
            this.lblApellido.TabIndex = 3;
            this.lblApellido.Text = "Apellido";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(8, 20);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(44, 13);
            this.lblNombre.TabIndex = 1;
            this.lblNombre.Text = "Nombre";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(149, 19);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 13;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(232, 19);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 12;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // grbDomicilio
            // 
            this.grbDomicilio.Controls.Add(this.txtDepto);
            this.grbDomicilio.Controls.Add(this.txtPiso);
            this.grbDomicilio.Controls.Add(this.txtNumero);
            this.grbDomicilio.Controls.Add(this.txtCalle);
            this.grbDomicilio.Controls.Add(this.lblDepto);
            this.grbDomicilio.Controls.Add(this.lblPiso);
            this.grbDomicilio.Controls.Add(this.lblNumero);
            this.grbDomicilio.Controls.Add(this.lblCalle);
            this.grbDomicilio.Location = new System.Drawing.Point(13, 193);
            this.grbDomicilio.Name = "grbDomicilio";
            this.grbDomicilio.Size = new System.Drawing.Size(335, 105);
            this.grbDomicilio.TabIndex = 6;
            this.grbDomicilio.TabStop = false;
            this.grbDomicilio.Text = "Domicilio";
            // 
            // lblDepto
            // 
            this.lblDepto.AutoSize = true;
            this.lblDepto.Location = new System.Drawing.Point(221, 58);
            this.lblDepto.Name = "lblDepto";
            this.lblDepto.Size = new System.Drawing.Size(74, 13);
            this.lblDepto.TabIndex = 9;
            this.lblDepto.Text = "Departamento";
            // 
            // lblPiso
            // 
            this.lblPiso.AutoSize = true;
            this.lblPiso.Location = new System.Drawing.Point(119, 58);
            this.lblPiso.Name = "lblPiso";
            this.lblPiso.Size = new System.Drawing.Size(27, 13);
            this.lblPiso.TabIndex = 7;
            this.lblPiso.Text = "Piso";
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Location = new System.Drawing.Point(9, 58);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(44, 13);
            this.lblNumero.TabIndex = 5;
            this.lblNumero.Text = "Numero";
            // 
            // lblCalle
            // 
            this.lblCalle.AutoSize = true;
            this.lblCalle.Location = new System.Drawing.Point(9, 16);
            this.lblCalle.Name = "lblCalle";
            this.lblCalle.Size = new System.Drawing.Size(30, 13);
            this.lblCalle.TabIndex = 3;
            this.lblCalle.Text = "Calle";
            // 
            // grbComandos
            // 
            this.grbComandos.Controls.Add(this.btnCancelar);
            this.grbComandos.Controls.Add(this.btnAceptar);
            this.grbComandos.Location = new System.Drawing.Point(13, 304);
            this.grbComandos.Name = "grbComandos";
            this.grbComandos.Size = new System.Drawing.Size(335, 57);
            this.grbComandos.TabIndex = 11;
            this.grbComandos.TabStop = false;
            this.grbComandos.Text = "Acciones";
            // 
            // txtDepto
            // 
            this.txtDepto.ActiveEnterTab = true;
            this.txtDepto.BackColor = System.Drawing.Color.Transparent;
            this.txtDepto.IsOptional = true;
            this.txtDepto.IsValid = true;
            this.txtDepto.Location = new System.Drawing.Point(223, 71);
            this.txtDepto.Margin = new System.Windows.Forms.Padding(0);
            this.txtDepto.MaxLength = 2;
            this.txtDepto.Name = "txtDepto";
            this.txtDepto.OnlyNumbers = false;
            this.txtDepto.Padding = new System.Windows.Forms.Padding(2);
            this.txtDepto.Size = new System.Drawing.Size(89, 24);
            this.txtDepto.TabIndex = 10;
            this.txtDepto.Value = "";
            // 
            // txtPiso
            // 
            this.txtPiso.ActiveEnterTab = true;
            this.txtPiso.BackColor = System.Drawing.Color.Transparent;
            this.txtPiso.IsOptional = true;
            this.txtPiso.IsValid = true;
            this.txtPiso.Location = new System.Drawing.Point(122, 71);
            this.txtPiso.Margin = new System.Windows.Forms.Padding(0);
            this.txtPiso.MaxLength = 2;
            this.txtPiso.Name = "txtPiso";
            this.txtPiso.OnlyNumbers = true;
            this.txtPiso.Padding = new System.Windows.Forms.Padding(2);
            this.txtPiso.Size = new System.Drawing.Size(74, 24);
            this.txtPiso.TabIndex = 9;
            this.txtPiso.Value = "";
            // 
            // txtNumero
            // 
            this.txtNumero.ActiveEnterTab = true;
            this.txtNumero.BackColor = System.Drawing.Color.Transparent;
            this.txtNumero.IsOptional = false;
            this.txtNumero.IsValid = true;
            this.txtNumero.Location = new System.Drawing.Point(9, 71);
            this.txtNumero.Margin = new System.Windows.Forms.Padding(0);
            this.txtNumero.MaxLength = 5;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.OnlyNumbers = true;
            this.txtNumero.Padding = new System.Windows.Forms.Padding(2);
            this.txtNumero.Size = new System.Drawing.Size(81, 24);
            this.txtNumero.TabIndex = 8;
            this.txtNumero.Value = "";
            // 
            // txtCalle
            // 
            this.txtCalle.ActiveEnterTab = true;
            this.txtCalle.BackColor = System.Drawing.Color.Transparent;
            this.txtCalle.IsOptional = false;
            this.txtCalle.IsValid = true;
            this.txtCalle.Location = new System.Drawing.Point(9, 34);
            this.txtCalle.Margin = new System.Windows.Forms.Padding(0);
            this.txtCalle.MaxLength = 50;
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.OnlyNumbers = false;
            this.txtCalle.Padding = new System.Windows.Forms.Padding(2);
            this.txtCalle.Size = new System.Drawing.Size(303, 24);
            this.txtCalle.TabIndex = 7;
            this.txtCalle.Value = "";
            // 
            // txtApellido
            // 
            this.txtApellido.ActiveEnterTab = true;
            this.txtApellido.BackColor = System.Drawing.Color.Transparent;
            this.txtApellido.IsOptional = false;
            this.txtApellido.IsValid = true;
            this.txtApellido.Location = new System.Drawing.Point(9, 73);
            this.txtApellido.Margin = new System.Windows.Forms.Padding(0);
            this.txtApellido.MaxLength = 40;
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.OnlyNumbers = false;
            this.txtApellido.Padding = new System.Windows.Forms.Padding(2);
            this.txtApellido.Size = new System.Drawing.Size(305, 24);
            this.txtApellido.TabIndex = 2;
            this.txtApellido.Value = "";
            // 
            // txtNombre
            // 
            this.txtNombre.ActiveEnterTab = true;
            this.txtNombre.BackColor = System.Drawing.Color.Transparent;
            this.txtNombre.IsOptional = false;
            this.txtNombre.IsValid = true;
            this.txtNombre.Location = new System.Drawing.Point(9, 33);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(0);
            this.txtNombre.MaxLength = 30;
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.OnlyNumbers = false;
            this.txtNombre.Padding = new System.Windows.Forms.Padding(2);
            this.txtNombre.Size = new System.Drawing.Size(305, 24);
            this.txtNombre.TabIndex = 1;
            this.txtNombre.Value = "";
            // 
            // txtDni
            // 
            this.txtDni.ActiveEnterTab = true;
            this.txtDni.BackColor = System.Drawing.Color.Transparent;
            this.txtDni.IsOptional = false;
            this.txtDni.IsValid = true;
            this.txtDni.Location = new System.Drawing.Point(9, 113);
            this.txtDni.Margin = new System.Windows.Forms.Padding(0);
            this.txtDni.MaxLength = 8;
            this.txtDni.Name = "txtDni";
            this.txtDni.OnlyNumbers = true;
            this.txtDni.Padding = new System.Windows.Forms.Padding(2);
            this.txtDni.Size = new System.Drawing.Size(68, 24);
            this.txtDni.TabIndex = 3;
            this.txtDni.Value = "";
            // 
            // txtEdad
            // 
            this.txtEdad.ActiveEnterTab = true;
            this.txtEdad.BackColor = System.Drawing.Color.Transparent;
            this.txtEdad.IsOptional = false;
            this.txtEdad.IsValid = true;
            this.txtEdad.Location = new System.Drawing.Point(90, 113);
            this.txtEdad.Margin = new System.Windows.Forms.Padding(0);
            this.txtEdad.MaxLength = 3;
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.OnlyNumbers = true;
            this.txtEdad.Padding = new System.Windows.Forms.Padding(2);
            this.txtEdad.Size = new System.Drawing.Size(42, 24);
            this.txtEdad.TabIndex = 4;
            this.txtEdad.Value = "";
            // 
            // chkActivo
            // 
            this.chkActivo.AutoSize = true;
            this.chkActivo.Location = new System.Drawing.Point(11, 145);
            this.chkActivo.Name = "chkActivo";
            this.chkActivo.Size = new System.Drawing.Size(169, 17);
            this.chkActivo.TabIndex = 9;
            this.chkActivo.Text = "El alumno se encuentra activo";
            this.chkActivo.UseVisualStyleBackColor = true;
            // 
            // frmAlumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 373);
            this.Controls.Add(this.grbComandos);
            this.Controls.Add(this.grbDomicilio);
            this.Controls.Add(this.grbDatosAlumno);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "frmAlumnos";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "--------------- Alta de Alumnos";
            this.Load += new System.EventHandler(this.frmAlumnos_Load);
            this.grbDatosAlumno.ResumeLayout(false);
            this.grbDatosAlumno.PerformLayout();
            this.grbDomicilio.ResumeLayout(false);
            this.grbDomicilio.PerformLayout();
            this.grbComandos.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbDatosAlumno;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.GroupBox grbDomicilio;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.DateTimePicker dtpFDN;
        private System.Windows.Forms.Label lblDni;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.Label lblApellido;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblCalle;
        private System.Windows.Forms.Label lblPiso;
        private System.Windows.Forms.GroupBox grbComandos;
        private System.Windows.Forms.Label lblFDN;
        private System.Windows.Forms.Label lblDepto;
        private UserControls.TextInput txtEdad;
        private UserControls.TextInput txtDni;
        private UserControls.TextInput txtApellido;
        private UserControls.TextInput txtNombre;
        private UserControls.TextInput txtCalle;
        private UserControls.TextInput txtNumero;
        private UserControls.TextInput txtDepto;
        private UserControls.TextInput txtPiso;
        private System.Windows.Forms.CheckBox chkActivo;
    }
}