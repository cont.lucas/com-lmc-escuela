﻿using Com.Lmc.Escuela.Entidades;
using Com.Lmc.Escuela.Presentacion.UserControls;
using Com.Lmc.Escuela.Repositorio;
using LiteDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace Com.Lmc.Escuela.Presentacion.Forms.Alumnos {
    public partial class frmAlumnos : Form {
        private List<bool> _fieldsValidated;
        private bool _finalResult = false;
        private bool _isEdition = false;

        private Alumno _alumno = new Alumno();

        public frmAlumnos() {
            InitializeComponent();
        }

        public frmAlumnos(Alumno alumno) : this() {
            _isEdition = true;
            _alumno = alumno;
        }

        private void InitBinding() {
            txtNombre.DataBindingsTextInput.Add("Text", _alumno, "Nombre", false, DataSourceUpdateMode.OnPropertyChanged);
            txtApellido.DataBindingsTextInput.Add("Text", _alumno, "Apellido", false, DataSourceUpdateMode.OnPropertyChanged);
            txtDni.DataBindingsTextInput.Add("Text", _alumno, "Dni", false, DataSourceUpdateMode.OnPropertyChanged);
            txtEdad.DataBindingsTextInput.Add("Text", _alumno, "Edad", true, DataSourceUpdateMode.OnPropertyChanged);

            txtCalle.DataBindingsTextInput.Add("Text", _alumno.Domicilio, "Calle", false, DataSourceUpdateMode.OnPropertyChanged);
            txtNumero.DataBindingsTextInput.Add("Text", _alumno.Domicilio, "Numero", true, DataSourceUpdateMode.OnPropertyChanged);
            txtPiso.DataBindingsTextInput.Add("Text", _alumno.Domicilio, "Piso", true, DataSourceUpdateMode.OnPropertyChanged);
            txtDepto.DataBindingsTextInput.Add("Text", _alumno.Domicilio, "Departamento", true, DataSourceUpdateMode.OnPropertyChanged);

            chkActivo.DataBindings.Add("Checked", _alumno, "Activo", true, DataSourceUpdateMode.OnPropertyChanged);
            dtpFDN.DataBindings.Add("Value", _alumno, "FechaNacimiento", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void ValidateOnSubmit(Control.ControlCollection collection = null) {
            if (collection == null) {
                this._fieldsValidated = new List<bool>();
                ValidateOnSubmit(Controls);
            }
            else {
                foreach (Control control in collection) {
                    var txt = control as TextInput;

                    if (control.Controls.Count > 0) {
                        if (txt != null) {
                            txt.IsValid = txt.HasAnyValue();
                            _fieldsValidated.Add(txt.IsValid);
                        }
                        else {
                            ValidateOnSubmit(control.Controls);
                        }
                    }
                }
            }

            this._finalResult = _fieldsValidated.Contains(false);
        }

        private void btnCancelar_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void frmAlumnos_Load(object sender, EventArgs e) {
            InitBinding();
        }

        private void btnAceptar_Click(object sender, EventArgs e) {
            ValidateOnSubmit();

            /*validaciones que pasaron correctamente*/
            if (_finalResult == false) {
                /*guardar en la base de datos*/
                try {
                    using (var db = new LiteDatabase(ConfigurationManager.AppSettings["dbname"])) {
                        var repo = new RepositorioLiteDb<Alumno>(db);
                        var message = "Alumno {0} correctamente";

                        if (_isEdition) {
                            repo.Update(_alumno);
                            message = message.Replace("{0}", "editado");
                        }
                        else {
                            repo.Insert(_alumno);
                            message = message.Replace("{0}", "creado");
                        }

                        MessageBox.Show(message, "Alumnos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DialogResult = DialogResult.OK;
                        Close();
                    }
                }
                catch (Exception ex) {
                    /*crear log de la excepcion*/
                    MessageBox.Show("Ups!, se produjo un error");
                }
            }
        }
    }
}