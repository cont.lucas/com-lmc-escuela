﻿using Com.Lmc.Escuela.Entidades;
using Com.Lmc.Escuela.Repositorio;
using LiteDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Com.Lmc.Escuela.Presentacion.Forms.Alumnos {
    public partial class frmListaAlumnos : Form {
        private List<Alumno> _alumnos;

        public frmListaAlumnos() {
            InitializeComponent();

            this.dgvAlumnos.AutoGenerateColumns = false;
        }

        private void frmListaAlumnos_Load(object sender, EventArgs e) {
            GetAndBindAlumnos();
        }

        private void btnBuscar_Click(object sender, EventArgs e) {
            var domi = this.txtCalleNumPisoDepto.Value;
            var alu = this.txtNomApeDni.Value;

            if (Utils.Utils.IsEmptyString(domi) && Utils.Utils.IsEmptyString(alu)) {
                this.dgvAlumnos.DataSource = this._alumnos;
            }
            else {
                var query = (from alumno in this._alumnos
                             where alumno.Domicilio.ExistDomicilio(domi)
                                && alumno.ExistAlumno(alu)
                             select alumno);

                var result = query.ToList();
                this.dgvAlumnos.DataSource = result;

                if (result.Count == 0) {
                    MessageBox.Show("No se han encontrado alumnos", "Listado de alumnos", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            this.txtCalleNumPisoDepto.Value = "";
            this.txtNomApeDni.Value = "";
        }

        private void btnAgregarAlumno_Click(object sender, EventArgs e) {
            var fa = new frmAlumnos();
            var result = fa.ShowDialog(this);

            if (result == DialogResult.OK) {
                GetAndBindAlumnos();
            }
        }

        private void dgvAlumnos_CellClick(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex > -1 && e.ColumnIndex == 0) {
                var currentAlumno = this._alumnos[e.RowIndex];

                var fa = new frmAlumnos(currentAlumno);
                var result = fa.ShowDialog(this);

                if (result == DialogResult.OK) {
                    GetAndBindAlumnos();
                }
            }
        }

        private void GetAndBindAlumnos() {
            using (var db = new LiteDatabase(ConfigurationManager.AppSettings["dbname"])) {
                var repo = new RepositorioLiteDb<Alumno>(db);

                this._alumnos = repo.GetAll().ToList();
            }

            this.dgvAlumnos.DataSource = this._alumnos;
        }
    }
}
