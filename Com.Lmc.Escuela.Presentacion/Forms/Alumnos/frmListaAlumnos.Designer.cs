﻿namespace Com.Lmc.Escuela.Presentacion.Forms.Alumnos {
    partial class frmListaAlumnos {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnAgregarAlumno = new System.Windows.Forms.Button();
            this.grbAlumnos = new System.Windows.Forms.GroupBox();
            this.dgvAlumnos = new System.Windows.Forms.DataGridView();
            this.grbFiltros = new System.Windows.Forms.GroupBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.lblCriterioDomicilio = new System.Windows.Forms.Label();
            this.lblCriterio = new System.Windows.Forms.Label();
            this.txtCalleNumPisoDepto = new Com.Lmc.Escuela.Presentacion.UserControls.TextInput();
            this.txtNomApeDni = new Com.Lmc.Escuela.Presentacion.UserControls.TextInput();
            this.colEditar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colApellido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEdad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colActivo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colDomicilio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grbAlumnos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlumnos)).BeginInit();
            this.grbFiltros.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAgregarAlumno
            // 
            this.btnAgregarAlumno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAgregarAlumno.Location = new System.Drawing.Point(484, 12);
            this.btnAgregarAlumno.Name = "btnAgregarAlumno";
            this.btnAgregarAlumno.Size = new System.Drawing.Size(111, 26);
            this.btnAgregarAlumno.TabIndex = 0;
            this.btnAgregarAlumno.Text = "Agregar Alumno";
            this.btnAgregarAlumno.UseVisualStyleBackColor = true;
            this.btnAgregarAlumno.Click += new System.EventHandler(this.btnAgregarAlumno_Click);
            // 
            // grbAlumnos
            // 
            this.grbAlumnos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbAlumnos.Controls.Add(this.dgvAlumnos);
            this.grbAlumnos.Location = new System.Drawing.Point(12, 114);
            this.grbAlumnos.Name = "grbAlumnos";
            this.grbAlumnos.Padding = new System.Windows.Forms.Padding(10);
            this.grbAlumnos.Size = new System.Drawing.Size(583, 224);
            this.grbAlumnos.TabIndex = 5;
            this.grbAlumnos.TabStop = false;
            this.grbAlumnos.Text = "Listado de Alumnos";
            // 
            // dgvAlumnos
            // 
            this.dgvAlumnos.AllowUserToAddRows = false;
            this.dgvAlumnos.AllowUserToDeleteRows = false;
            this.dgvAlumnos.AllowUserToOrderColumns = true;
            this.dgvAlumnos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvAlumnos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvAlumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvAlumnos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colEditar,
            this.colId,
            this.colNombre,
            this.colApellido,
            this.colDni,
            this.colEdad,
            this.colActivo,
            this.colDomicilio});
            this.dgvAlumnos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAlumnos.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvAlumnos.Location = new System.Drawing.Point(10, 23);
            this.dgvAlumnos.Name = "dgvAlumnos";
            this.dgvAlumnos.ReadOnly = true;
            this.dgvAlumnos.RowHeadersVisible = false;
            this.dgvAlumnos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvAlumnos.Size = new System.Drawing.Size(563, 191);
            this.dgvAlumnos.TabIndex = 2;
            this.dgvAlumnos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAlumnos_CellClick);
            // 
            // grbFiltros
            // 
            this.grbFiltros.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbFiltros.Controls.Add(this.btnBuscar);
            this.grbFiltros.Controls.Add(this.txtCalleNumPisoDepto);
            this.grbFiltros.Controls.Add(this.lblCriterioDomicilio);
            this.grbFiltros.Controls.Add(this.txtNomApeDni);
            this.grbFiltros.Controls.Add(this.lblCriterio);
            this.grbFiltros.Location = new System.Drawing.Point(12, 38);
            this.grbFiltros.Name = "grbFiltros";
            this.grbFiltros.Size = new System.Drawing.Size(583, 70);
            this.grbFiltros.TabIndex = 1;
            this.grbFiltros.TabStop = false;
            this.grbFiltros.Text = "Filtros";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnBuscar.BackColor = System.Drawing.SystemColors.Control;
            this.btnBuscar.BackgroundImage = global::Com.Lmc.Escuela.Presentacion.Properties.Resources.searchicon;
            this.btnBuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBuscar.Location = new System.Drawing.Point(527, 22);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(35, 35);
            this.btnBuscar.TabIndex = 4;
            this.btnBuscar.UseVisualStyleBackColor = false;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // lblCriterioDomicilio
            // 
            this.lblCriterioDomicilio.AutoSize = true;
            this.lblCriterioDomicilio.Location = new System.Drawing.Point(263, 20);
            this.lblCriterioDomicilio.Name = "lblCriterioDomicilio";
            this.lblCriterioDomicilio.Size = new System.Drawing.Size(247, 13);
            this.lblCriterioDomicilio.TabIndex = 2;
            this.lblCriterioDomicilio.Text = "Busqueda por Calle, Número, Piso o Departamento";
            // 
            // lblCriterio
            // 
            this.lblCriterio.AutoSize = true;
            this.lblCriterio.Location = new System.Drawing.Point(9, 20);
            this.lblCriterio.Name = "lblCriterio";
            this.lblCriterio.Size = new System.Drawing.Size(184, 13);
            this.lblCriterio.TabIndex = 0;
            this.lblCriterio.Text = "Busqueda por Nombre, Apellido o Dni";
            // 
            // txtCalleNumPisoDepto
            // 
            this.txtCalleNumPisoDepto.ActiveEnterTab = true;
            this.txtCalleNumPisoDepto.BackColor = System.Drawing.Color.Transparent;
            this.txtCalleNumPisoDepto.IsOptional = false;
            this.txtCalleNumPisoDepto.IsValid = true;
            this.txtCalleNumPisoDepto.Location = new System.Drawing.Point(264, 33);
            this.txtCalleNumPisoDepto.Margin = new System.Windows.Forms.Padding(0);
            this.txtCalleNumPisoDepto.MaxLength = 0;
            this.txtCalleNumPisoDepto.Name = "txtCalleNumPisoDepto";
            this.txtCalleNumPisoDepto.OnlyNumbers = false;
            this.txtCalleNumPisoDepto.Padding = new System.Windows.Forms.Padding(2);
            this.txtCalleNumPisoDepto.Size = new System.Drawing.Size(244, 24);
            this.txtCalleNumPisoDepto.TabIndex = 5;
            this.txtCalleNumPisoDepto.Value = "";
            // 
            // txtNomApeDni
            // 
            this.txtNomApeDni.ActiveEnterTab = true;
            this.txtNomApeDni.BackColor = System.Drawing.Color.Transparent;
            this.txtNomApeDni.IsOptional = false;
            this.txtNomApeDni.IsValid = true;
            this.txtNomApeDni.Location = new System.Drawing.Point(10, 33);
            this.txtNomApeDni.Margin = new System.Windows.Forms.Padding(0);
            this.txtNomApeDni.MaxLength = 0;
            this.txtNomApeDni.Name = "txtNomApeDni";
            this.txtNomApeDni.OnlyNumbers = false;
            this.txtNomApeDni.Padding = new System.Windows.Forms.Padding(2);
            this.txtNomApeDni.Size = new System.Drawing.Size(244, 24);
            this.txtNomApeDni.TabIndex = 6;
            this.txtNomApeDni.Value = "";
            // 
            // colEditar
            // 
            this.colEditar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.NullValue = "Editar";
            this.colEditar.DefaultCellStyle = dataGridViewCellStyle1;
            this.colEditar.HeaderText = "";
            this.colEditar.Name = "colEditar";
            this.colEditar.ReadOnly = true;
            this.colEditar.Width = 5;
            // 
            // colId
            // 
            this.colId.DataPropertyName = "Id";
            this.colId.HeaderText = "Id";
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            this.colId.Visible = false;
            this.colId.Width = 41;
            // 
            // colNombre
            // 
            this.colNombre.DataPropertyName = "Nombre";
            this.colNombre.HeaderText = "Nombre";
            this.colNombre.Name = "colNombre";
            this.colNombre.ReadOnly = true;
            this.colNombre.Width = 69;
            // 
            // colApellido
            // 
            this.colApellido.DataPropertyName = "Apellido";
            this.colApellido.HeaderText = "Apellido";
            this.colApellido.Name = "colApellido";
            this.colApellido.ReadOnly = true;
            this.colApellido.Width = 69;
            // 
            // colDni
            // 
            this.colDni.DataPropertyName = "Dni";
            this.colDni.HeaderText = "Dni";
            this.colDni.Name = "colDni";
            this.colDni.ReadOnly = true;
            this.colDni.Width = 48;
            // 
            // colEdad
            // 
            this.colEdad.DataPropertyName = "Edad";
            this.colEdad.HeaderText = "Edad";
            this.colEdad.Name = "colEdad";
            this.colEdad.ReadOnly = true;
            this.colEdad.Width = 57;
            // 
            // colActivo
            // 
            this.colActivo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.colActivo.DataPropertyName = "Activo";
            this.colActivo.HeaderText = "Activo";
            this.colActivo.Name = "colActivo";
            this.colActivo.ReadOnly = true;
            this.colActivo.Width = 43;
            // 
            // colDomicilio
            // 
            this.colDomicilio.DataPropertyName = "DomicilioFull";
            this.colDomicilio.HeaderText = "Domicilio";
            this.colDomicilio.Name = "colDomicilio";
            this.colDomicilio.ReadOnly = true;
            this.colDomicilio.Width = 200;
            // 
            // frmListaAlumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 350);
            this.Controls.Add(this.btnAgregarAlumno);
            this.Controls.Add(this.grbFiltros);
            this.Controls.Add(this.grbAlumnos);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(615, 377);
            this.Name = "frmListaAlumnos";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "--------------- Listado de Alumnos";
            this.Load += new System.EventHandler(this.frmListaAlumnos_Load);
            this.grbAlumnos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlumnos)).EndInit();
            this.grbFiltros.ResumeLayout(false);
            this.grbFiltros.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnAgregarAlumno;
        private System.Windows.Forms.GroupBox grbAlumnos;
        private System.Windows.Forms.GroupBox grbFiltros;
        private UserControls.TextInput txtNomApeDni;
        private System.Windows.Forms.Label lblCriterio;
        private UserControls.TextInput txtCalleNumPisoDepto;
        private System.Windows.Forms.Label lblCriterioDomicilio;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.DataGridView dgvAlumnos;
        private System.Windows.Forms.DataGridViewButtonColumn colEditar;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn colApellido;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDni;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEdad;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colActivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDomicilio;
    }
}