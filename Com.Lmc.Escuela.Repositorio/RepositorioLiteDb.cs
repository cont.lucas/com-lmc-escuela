﻿using Com.Lmc.Escuela.Repositorio.Contratos;
using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Com.Lmc.Escuela.Repositorio {
    public class RepositorioLiteDb<T> : IRepositorio<T> where T : class {
        protected LiteCollection<T> entidad;

        public RepositorioLiteDb(LiteDatabase db) {
            this.entidad = db.GetCollection<T>();
        }

        public int Insert(T entidad) {
            return this.entidad.Insert(entidad);
        }

        public bool Update(T entidad) {
            return this.entidad.Update(entidad);
        }

        public void Delete(T entidad) {
            throw new NotImplementedException();
        }

        public IQueryable<T> SearchFor(Expression<Func<T, bool>> expression) {
            throw new NotImplementedException();
        }

        public IQueryable<T> GetAll() {
            return this.entidad.FindAll().AsQueryable();
        }

        public T GetOne(int id) {
            throw new NotImplementedException();
        }
    }
}
