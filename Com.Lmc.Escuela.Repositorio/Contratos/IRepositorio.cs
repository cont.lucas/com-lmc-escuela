﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Com.Lmc.Escuela.Repositorio.Contratos {
    public interface IRepositorio<T> {
        int Insert(T entity);
        bool Update(T entity);
        void Delete(T entity);
        IQueryable<T> GetAll();
        IQueryable<T> SearchFor(Expression<Func<T, bool>> expression);
        T GetOne(int id);
    }
}
