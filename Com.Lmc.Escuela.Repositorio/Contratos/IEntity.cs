﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Lmc.Escuela.Repositorio.Contratos {
    public interface IEntity {
        int ID { get; }
    }
}
