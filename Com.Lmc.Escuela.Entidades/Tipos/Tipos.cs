﻿public enum TipoProfesional {
    Maestro = 0,
    Director = 1
}

public enum TipoTelefono {
    Casa = 0,
    Fax = 1,
    Celular = 2
}