﻿using Com.Lmc.Escuela.Entidades.Contratos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Lmc.Escuela.Entidades {
    public class Alumno : IPersona {
        private int? _edad;
        public Alumno() {
            this.Domicilio = new Domicilio();
        }
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Dni { get; set; }
        public int? Edad {
            get { return this._edad; }
            set {
                if (value.HasValue) {
                    FechaNacimiento = DateTime.Now.AddYears(-value.Value);
                }
                this._edad = value;
            }
        }
        public DateTime FechaNacimiento { get; set; }
        public List<string> Telefonos { get; set; }

        public Domicilio Domicilio { get; set; }
        public bool Activo { get; set; } = true;
        public string DomicilioFull {
            get {
                return Domicilio.Calle
                    + " " + Domicilio.Numero.ToString()
                    + " Piso: " + Domicilio.Piso
                    + " Departamento: " + Domicilio.Departamento;
            }
        }

        public bool ExistAlumno(string param) {
            if (String.IsNullOrEmpty(param) || string.IsNullOrWhiteSpace(param)) {
                return true;
            }

            string cadena = this.Nombre + this.Apellido + this.Dni;
            cadena = cadena.Trim().ToLower();

            var result = (cadena.IndexOf(param.ToLower()) >= 0);
            return result;
        }
    }
}
