﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Lmc.Escuela.Entidades {
    public class Domicilio {
        public Domicilio() {
            Localidad = new Localidad();
            Provincia = new Provincia();
        }
        public String Calle { get; set; }
        public Int32? Numero { get; set; }
        public String Departamento { get; set; }
        public String Piso { get; set; }
        public Localidad Localidad { get; set; }
        public Provincia Provincia { get; set; }

        public bool ExistDomicilio(string param) {
            if (String.IsNullOrEmpty(param) || string.IsNullOrWhiteSpace(param)) {
                return true;
            }

            string cadena = this.Calle + this.Numero.ToString() + this.Piso + this.Departamento;
            cadena = cadena.Trim().ToLower();

            var result = (cadena.IndexOf(param.ToLower()) >= 0);
            return result;
        }
    }
}
