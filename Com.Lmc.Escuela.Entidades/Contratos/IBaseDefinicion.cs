﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Lmc.Escuela.Entidades.Contratos {
    public interface IBaseDefinicion {
        Int32 Id { get; set; }
        String Codigo { get; set; }
        String Descripcion { get; set; }
    }
}
