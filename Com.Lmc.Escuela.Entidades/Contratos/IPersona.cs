﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Lmc.Escuela.Entidades.Contratos {
    public interface IPersona {
        String Nombre { get; set; }
        String Apellido { get; set; }
        String Dni { get; set; }
        List<String> Telefonos { get; set; }
    }
}
