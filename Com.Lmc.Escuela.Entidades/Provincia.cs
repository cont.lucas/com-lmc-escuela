﻿using Com.Lmc.Escuela.Entidades.Contratos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Lmc.Escuela.Entidades {
    public class Provincia : IBaseDefinicion {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
