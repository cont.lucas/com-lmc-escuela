﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Lmc.Escuela.Entidades {
    public class Escuela {
        public string Nombre { get; set; }
        public Domicilio Domicilio { get; set; }
    }
}
